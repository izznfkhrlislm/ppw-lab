window.fbAsyncInit = () => {
	FB.init({
		appId	: '1872647112767145',
		cookie	: true,
		xfbml	: true,
		version : 'v2.11'
	});

	FB.AppEvents.logPageView();

	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			render(true);
		}
		else {
			render(false);
		}
	});
};

(function (d,s,id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)){
		return;
	}
	js = d.createElement(s);
	js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

const render = (loginFlag) => {
	if (loginFlag){
		getUserData(user => {
			$('#lab8').html(
				'<div class="coverandpic">'+
					'<img class="picture" src="' + user.picture.data.url + '" alt="profpic" width="150px" height="150px" style="display:block; margin: 0 auto"/>' +
					'<img class="cover" src="' + user.cover.source + '" alt="cover" width="100%" style="clip: rect(0px,50px,50px,50px);"/>' +
				'</div>' +
				'<div class="userandabout">' +
					'<h1 align="center" style="font-weight:bold; color:white;">' + user.name + ' <a onclick="facebookLogout()"><i class="fa fa-sign-out" aria-hidden="true"></i></a>'+ '</h1>' +      
              		'<p align="center" style="font-weight:bold; color:white; font-size:17px; word-wrap: break-word;">' + user.about + '</p>' +
              	'</div>' +
              	'<hr/>' +
              	'<div class="container" id="emailgen">'+ 
              		'<p align="center" style="font-weight:bold; font-size:17px;"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:' + user.email +'">'+ user.email +'</a>' +' | '+ '  <i class="fa fa-venus-mars" aria-hidden="true"></i> ' + user.gender + '</p>' + 
		        '</div>'+
		        '<br>'+
		        '<section name="status-input" id="status-input" align="center">'+
		        	'<div>'+
		        		'<textarea class="form-control" id="postInput" type="text" rows="4" class="post" placeholder="What&apos;s on your mind?" />' +
		        		'<br>'+
		        		'<button class="btn btn-primary" onclick="postStatus()">Post to Facebook</button>' +
		        	'</div>'+
		        '</section>'+
		        '<h2 style="font-weight: bold;">'+ user.name +'&apos;s Recent Posts</h4>'+
		        '<hr/>'
		        //'<button class="logout" onclick="facebookLogout()">Logout</button>'

			);

			getUserFeed(feed => {
				feed.data.map(value => {
					if (value.message && value.story){
						$('#lab8').append(
							'<div class="card">' +
                  				'<div class="card-body">' + value.message + '</div>' +
                  				'<br>'+
                  				'<div class="card-body">' + value.story + '</div>' +
                  				'<br>'+
                			'</div>'
						);
					}
					else if (value.message){
						$('#lab8').append(
							'<div class="card">' +
								'<br>'+
                  				'<div class="card-body">' + value.message + '</div>' +
                  				'<br>'+
                			'</div>'
						);
					}
					else if (value.story){
						$('#lab8').append(
							'<div class="card">' +
								'<br>'+
                  				'<div class="card-body">' + value.story + '</div>' +
                  				'<br>'+
                			'</div>'
						);
					}
				});
			});
		});
	}
	else {
		$('#lab8').html('<div id="login-btn">' +
            '<button class="btn btn-primary" onclick="facebookLogin()">Login with Facebook</button>' +
            '</div>');
	}
};

const facebookLogin = () => {
	FB.login(function(response){
		console.log(response);
		FB.api('/me/', {fields: 'name,email,picture.width(150).height(10)'}, function(response){
			console.log('Info: '+response.name+' '+response.email);
		});
		render(true);
	}, {scope:'public_profile,user_posts,publish_actions,user_about_me,email', auth_type:'rerequest'});
};

const facebookLogout = () => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.logout();
			console.log("Berhasil logout!");
			render(false);
		}
	})
};

const getUserData = (fun) => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.api('/me?fields=id,name,about,cover,email,picture,gender', 'GET', function(response){
				console.log(response);
				if (response && !response.error){
					//picture = response.picture.data.url;
					//name = response.name;
					//userID = response.authResponse.userID || response.authResponse.userId;
					fun(response);
				}
				else {
					alert("Error getting User Data!");
				}
			});
		}
	});
};

const getUserFeed = (fun) => {
	FB.getLoginStatus(function(response){
		if (response.status === 'connected'){
			FB.api('/me/posts/', 'get', function(response){
				console.log(response);
				if (response && !response.error){
					fun(response);
				}
				else {
					alert("Error getting Feed!");
				}
			});
		}
	});
};

const postFeed = (message) => {
	FB.api('/me/feed', 'post', {message:message});
	alert("Your post has been published!");
	window.location.reload();
};

const postStatus = () => {
	const message = $('#postInput').val();
	console.log(message);
	$('#postInput').val("");
	postFeed(message);
};

var picture, name, userID;