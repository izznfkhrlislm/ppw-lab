// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
	
	if (x === 'ac') {
	/* implemetnasi clear all */
	print.value = '';
	erase = true;
	} 

	else if (x === 'eval') {
		var res;
		try {
			res = Math.round(evil(print.value)*10000)/10000;
		}
		catch (SyntaxError){
			res = "Syntax Error";
		}
		if (res == Infinity || isNaN(res)) res = "Undefined"
		print.value = res;
		erase = true;
	}

	else if (x === 'log') {
		print.value = Math.log10(print.value);
	} 

	else if (x === 'sin'){
		print.value = Math.sin(print.value * Math.PI / 180);
		if (print.value == 1.2246467991473532e-16) print.value = 0;
	}

	else if (x === 'tan'){
		print.value = Math.tan(print.value * Math.PI / 180);
		if (print.value == -1.2246467991473532e-16) print.value = 0;
		if (print.value == 0.9999999999999999) print.value = 1;
	}

	else {
		print.value += x;
	}
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

const themes = [
					{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"black"},
					{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"black"},
					{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"black"},
					{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"black"},
					{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"black"},
					{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"black"},
					{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"black"},
					{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"black"},
					{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"black"},
					{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"black"},
					{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"black"}
				];

const defaultTheme = {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"black"};
var chatCS = "";
const chatBot = function(message){
	const corsProxy = 'https://cors-anywhere.herokuapp.com/';
	const botURL = 'http://www.cleverbot.com/getreply?key=CC5agXq7qqWbZxYhjlgWn8m9QZg&input='+message+"&cs="+chatCS;
	axios.get(corsProxy+botURL).then(function (response){
		console.log(response.data.output);
		chatCS = response.data.cs;
		$(".msg-insert").append("<p class='msg-receive'>"+response.data.output+"</p>");
		$(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast');
	});
}

$(document).ready(function() {
	
	if (!localStorage.themes){
		localStorage.themes = JSON.stringify(themes);
		localStorage.selectedTheme = JSON.stringify(defaultTheme);
	}

	$("#arrow").click(function(){
		var src = ($("#arrow").attr("src") === "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png") 
			? "https://png.icons8.com/collapse-arrow/win10/16/000000" 
			: "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png";
		$("#arrow").attr("src", src);
		$(".chat-body").slideToggle("slow");
	});

	$("#text").keypress(function(event) {
		/* Act on the event */
		if (event.keyCode == 13 && !event.shiftKey){
			event.preventDefault();
			var txt = $("#text").val();
			$(".msg-insert").append("<p class='msg-send'>"+txt+"</p>");
			//$(".msg-insert").append("<p class='msg-receive'>"+txt+"</p>");
			$(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast');
			$("#text").val("");
			chatBot(txt);
			return;
		}
	});
    
    $('#apply-button').on('click', function(){
    	//[TODO] ambil value dari elemen select .my-select
    	var data = JSON.parse(localStorage.themes);
    	var id = $(".my-select").select2("val");
    	//[TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    	$.each(data, function(i, theme){
    		if (id == theme.id){
    			//[TODO] ambil object yang dipilih
    			//[TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
    			$("body").css({"background-color" : theme.bcgColor, "color" : theme.fontColor});
    			//[TODO] simpan object theme tadi ke local storage selectedTheme
    			var selected = {
    				"id": theme.id,
    				"bcgColor": theme.bcgColor,
    				"fontColor": theme.fontColor
    			};
    			localStorage.selectedTheme = JSON.stringify(selected);
    			return false; //break the loop
    		}
    	})
    })

    $('.my-select').select2({
    	'data' : JSON.parse(localStorage.themes)
    });

    //Retrieve applied theme when browser is reopened
    var applied = JSON.parse(localStorage.selectedTheme);
    $("body").css({"background-color":applied.bcgColor, "color":applied.fontColor});
    //Change selection
    $(".my-select").val(JSON.parse(localStorage.selectedTheme).id).trigger('change');
});

