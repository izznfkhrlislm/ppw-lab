// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
	
	if (x === 'ac') {
	/* implemetnasi clear all */
	print.value = '';
	erase = true;
	} 

	else if (x === 'eval') {
		var res;
		try {
			res = Math.round(evil(print.value)*10000)/10000;
		}
		catch (SyntaxError){
			res = "Syntax Error";
		}
		if (res == Infinity || isNaN(res)) res = "Undefined"
		print.value = res;
		erase = true;
	} 

	else {
		print.value += x;
	}
};

function evil(fn) {
  return new Function('return ' + fn)();
}

// END
