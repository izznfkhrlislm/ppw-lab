from django.shortcuts import render
from lab_2.views import landing_page_content
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

# Create your views here.
response = {'author': "Izzan Fakhril Islam"} #TODO Implement your name
about_me = ['Likes to travel to random places by motorcycles',
            'A big fan of Musikal Petualangan Sherina and Musikal Laskar Pelangi',
            'Did not crack easily under pressure',
            'Quiet Room is my favorite place after class',
            'Likes to play Pro Evolution Soccer (but I am not pro at that)',
            'Linux #1 Fan!',
            ]
def index(request):
    response['content'] = landing_page_content
    html = 'lab_4.html'
    # TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda
    response['about_me'] = about_me
    response['message_form'] = Message_Form
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST)
    if request.method == 'POST' and form.is_valid():
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email = response['email'],
                          message=response['message'])
        message.save()
        html = 'form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/lab-4/')

def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'table.html'
    return render(request, html, response)