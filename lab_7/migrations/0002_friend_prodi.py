# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-15 08:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_7', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='prodi',
            field=models.CharField(default='-', max_length=50),
        ),
    ]
