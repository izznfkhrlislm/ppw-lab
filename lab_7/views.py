from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request): # pragma: no cover
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    friend_list = Friend.objects.all()
    auth = csui_helper.instance.get_auth_param_dict()

    page = request.GET.get('page', 1)
    paginate_data = paginate_web(page, mahasiswa_list)
    mahasiswa = paginate_data['data']
    page_range = paginate_data['page_range']
    response = {"mahasiswa_list": mahasiswa, "friend_list": friend_list, "page_range": page_range, "auth":auth}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def paginate_web(page, data_list): # pragma: no cover
    paginator = Paginator(data_list, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    index = data.number - 1
    max_index = len(paginator.page_range)
    start_index = index if index >= 10 else 0
    end_index = 10 if index < max_index - 10 else max_index

    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data

def friend_list(request): # pragma: no cover
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def get_friend_list(request): # pragma: no cover
    if request.method == 'GET':
        friend_list = Friend.objects.all()
        data = serializers.serialize('json', friend_list)
        return HttpResponse(data)

@csrf_exempt
def add_friend(request): # pragma: no cover
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        prodi = request.POST['prodi']
        is_taken = Friend.objects.filter(npm__iexact=npm).exists()
        if not is_taken:
            friend = Friend(friend_name=name, npm=npm, prodi=prodi)
            friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)

def delete_friend(request, friend_id): # pragma: no cover
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/friend-list/')

@csrf_exempt
def validate_npm(request): # pragma: no cover
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm__iexact=npm).exists() #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj): # pragma: no cover
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data

def friend_description(request, friend_id): # pragma: no cover
    friend = Friend.objects.filter(id=friend_id)[0]
    response['friend'] = friend
    html = 'lab_7/friend_description.html'
    return render(request, html, response)