from django.db import models

# Create your models here.
class Friend(models.Model):
	friend_name = models.CharField(max_length=400)
	npm = models.CharField(max_length=250)
	prodi = models.CharField(max_length=50)
	added_at = models.DateField(auto_now_add=True)
